import os
import joblib
import numpy as np
import pandas as pd
from sklearn.datasets import load_iris
from sklearn.neighbors import KNeighborsClassifier

from flask import Flask, request, flash, jsonify, abort, redirect, url_for, render_template, send_file, after_this_request
from flask_wtf import FlaskForm
from markupsafe import escape
from wtforms import StringField, FileField
from wtforms.validators import DataRequired
from werkzeug.utils import secure_filename

app = Flask(__name__)

# Using a development configuration
app.config.from_object('config.DevConfig')

@app.route('/')
def index():
    return render_template('introduction.html')

class MyForm(FlaskForm):
    file = FileField()

@app.route('/clear_format', methods=('GET', 'POST'))
def clear_format():
    """Clear excel format"""
    form = MyForm()
    if form.validate_on_submit():
        try:
            uploaded_file = form.file.data

            [file_handle, filename] = clead_format_function(uploaded_file)

            return send_file(file_handle, mimetype='text/xlsx', attachment_filename=filename, as_attachment=True)
        except Exception:
            return redirect(url_for('bad_request'))

    return render_template('clear_format.html', form=form)

@app.route('/sum_on_first_list', methods=('GET', 'POST'))
def sum_on_first_list():
    """Sum excel format"""
    form = MyForm()
    if form.validate_on_submit():
        try:
            uploaded_file = form.file.data

            counts = sum_function(uploaded_file)

            return str(counts)
        except Exception:
            return redirect(url_for('bad_request'))
    return render_template('sum_on_first_list.html', form=form)

@app.route('/count_lists', methods=('GET', 'POST'))
def count_lists():
    """Count lists"""
    form = MyForm()
    if form.validate_on_submit():
        try:
            uploaded_file = form.file.data

            count_lists = count_list_function(uploaded_file)

            return str(count_lists)
        except Exception:
            return redirect(url_for('bad_request'))
    return render_template('count_lists.html', form=form)

@app.route('/badrequest')
def bad_request():
    return abort(400)

def clead_format_function(uploaded_file):
    filename = secure_filename(uploaded_file.filename) + '_answers.xlsx'
    file_path = app.config['UPLOAD_FOLDER'] + '/' + filename

    df = pd.read_excel(uploaded_file, header=None)
    df.to_excel(file_path, index=False, header=None)

    #remove file after upload
    file_handle = open(file_path, 'r')

    @after_this_request 
    def remove_file(response): 
        os.remove(file_path) 
        return response

    return [file_handle, filename]

def sum_function(uploaded_file):

    df = pd.read_excel(uploaded_file, header=None)
    counts = df.values.sum()    

    return counts

def count_list_function(uploaded_file):

    xl = pd.ExcelFile(uploaded_file)
    res = len(xl.sheet_names) 

    return res