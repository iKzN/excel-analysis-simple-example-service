# Introduction

Hello, example work with excel in flask-microservice app

# run docker

## build and run
docker-compose up --build

# request

## upload file in interface
http://localhost:5001/clear_format - send file with 4 feature

## upload file by request
http://localhost:5001/sum_on_first_list - send file with 4 feature

## upload file by request
http://localhost:5001/count_lists - send file with 4 feature